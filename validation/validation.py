from functools import wraps
from os import path
from jsonschema import validate, ValidationError
from flask import request, jsonify
import yaml
from sqlalchemy.orm.exc import NoResultFound

with open(path.join(path.dirname(__file__), 'schema.yaml')) as f:
    schema = yaml.safe_load(f)


def validate_json(schema_one):
    def decorator(fun):
        @wraps(fun)
        def wrapper(*args, **kwargs):
            # [INIT WRAPPER]
            json = request.get_json(force=True)

            try:
                validate(json, schema_one)
                return fun(*args, *kwargs)

            # TODO: IMPROVE VALIDATIONS, AND AUTHORIZATIONS AND AUTHENTICATIONS
            except ValidationError as error:
                return jsonify(status='error', code=400, message=error.message)

            except NoResultFound:
                return jsonify(status='error', code=401, message="You're NOT allowed to use this service")
            # [END WRAPPER]
        return wrapper
    return decorator
