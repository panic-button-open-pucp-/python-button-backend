import random
    
def generate_salt(n=50):
    """ generate a random salt @return string(50) """
    ALPHABET = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
    
    chars=[]
    for i in range(n):
        chars.append(random.choice(ALPHABET))

    return "".join(chars)


def generate_hash(salt, password):
    salti = list(salt)
    # print("".join(salt))
    for (c1, c2) in zip(password, reversed(password)):
        salti[ord(c1)**2 % 50] = c2
        salti[ord(c1)**3 % 50] = c2
        salti[ord(c2)**2 % 50] = c1
        salti[ord(c2)**3 % 50] = c1
    # print("".join(salt))
    return "".join(salti)