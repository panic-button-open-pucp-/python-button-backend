from datetime import datetime

from flask import Flask, request, jsonify

from validation.validation import schema
from messaging.messaging import send_android_message
from validation.validation import validate_json

from database.crud import (
    register_emergency_db,
    get_guardians_tokens_db,
    keep_guardian_token_db,
    validate_login
)

app = Flask(__name__)


@app.route('/')
def main():
    return "API for OPEN PUCP"


@app.route('/api/alert', methods=['POST'])
@validate_json(schema['alert'])
def alert():
    # Receive json sent by button
    json = request.get_json(force=True)
    # Build data to send
    data = {
        'code': json['code'],
        'time': datetime.now().strftime("%H:%M:%S"),
        'date': datetime.now().strftime("%d-%m-%Y")
    }
    # Keep this emergency in database
    register_emergency_db(data)
    # Get all cellphone tokens
    tokens = get_guardians_tokens_db()
    for token in tokens:
        send_android_message(token, data)

    return jsonify(status='ok', code=200, message='alert sent to guards at OPEN PUCP')


@app.route('/api/guards', methods=['POST'])
@validate_json(schema['guard']['POST'])
def keep_guard_token():
    # receive json sent by smartphone
    json = request.get_json(force=True)
    # keep guardian token
    keep_guardian_token_db(json)
    return jsonify(status='ok', code=200, message='guard token kept in db successfully')


@app.route('/api/login', methods=['POST'])
@validate_json(schema['login'])
def login_guard():
    json = request.get_json(force=True)
    if validate_login(json):
        return jsonify(status='ok', code=200, message='logged in')
    else:
        return jsonify(status='error', code=401, message='no access')


if __name__ == "__main__":
    app.run(debug=False)
